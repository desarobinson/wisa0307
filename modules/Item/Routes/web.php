<?php

$hostname = app(Hyn\Tenancy\Contracts\CurrentHostname::class);

if($hostname) {
    Route::domain($hostname->fqdn)->group(function () {
        Route::middleware(['auth', 'locked.tenant'])->group(function() {


            Route::get('categories', 'CategoryController@index')->name('tenant.categories.index')->middleware('redirect.level');
            Route::get('categories/records', 'CategoryController@records');
            Route::get('categories/columns', 'CategoryController@columns');
            Route::get('categories/record/{category}', 'CategoryController@record');
            Route::post('categories', 'CategoryController@store');
            Route::delete('categories/{category}', 'CategoryController@destroy');


            Route::get('agentes', 'AgenteController@index')->name('tenant.agentes.index')->middleware('redirect.level');
            Route::get('agentes/records', 'AgenteController@records');
            Route::get('agentes/columns', 'AgenteController@columns');
            Route::get('/tables', 'AgenteController@tables');
            Route::get('agentes/record/{agente}', 'AgenteController@record');
            Route::post('agentes', 'AgenteController@store');
            Route::delete('agentes/{agente}', 'AgenteController@destroy');

            Route::get('cashagente', 'CashagenteController@index')->name('tenant.cashagente.index')->middleware('redirect.level');
            Route::get('cashagente/records', 'CashagenteController@records');
            Route::get('cashagente/columns', 'CashagenteController@columns');
            Route::get('/tables', 'CashagenteController@tables');
            Route::get('cashagente/record/{cashagente}', 'CashagenteController@record');
            Route::post('cashagente', 'CashagenteController@store');
            Route::delete('cashagente/{cashagente}', 'CashagenteController@destroy');
            Route::get('cashagente/cerrar/{cashagente}', 'CashagenteController@cerrar');
            Route::get('cashagente/report/{cash}', 'CashagenteController@report');

            Route::get('brands', 'BrandController@index')->name('tenant.brands.index')->middleware('redirect.level');
            Route::get('brands/records', 'BrandController@records');
            Route::get('brands/record/{brand}', 'BrandController@record');
            Route::post('brands', 'BrandController@store');
            Route::get('brands/columns', 'BrandController@columns');
            Route::delete('brands/{brand}', 'BrandController@destroy');

            Route::get('incentives', 'IncentiveController@index')->name('tenant.incentives.index')->middleware('redirect.level');
            Route::get('incentives/records', 'IncentiveController@records');
            Route::get('incentives/record/{incentive}', 'IncentiveController@record');
            Route::post('incentives', 'IncentiveController@store');
            Route::get('incentives/columns', 'IncentiveController@columns');
            Route::delete('incentives/{incentive}', 'IncentiveController@destroy');

            Route::get('items/barcode/{item}', 'ItemController@generateBarcode');

            Route::post('items/import/item-price-lists', 'ItemController@importItemPriceLists');

            Route::prefix('item-lots')->group(function () {
                
                Route::get('', 'ItemLotController@index')->name('tenant.item-lots.index');
                Route::get('/records', 'ItemLotController@records');
                Route::get('/record/{record}', 'ItemLotController@record');
                Route::post('', 'ItemLotController@store');
                Route::get('/columns', 'ItemLotController@columns');
                Route::get('/export', 'ItemLotController@export');

            });

            Route::post('items/import/item-sets', 'ItemSetController@importItemSets');
            Route::post('items/import/item-sets-individual', 'ItemSetController@importItemSetsIndividual');


        });
    });
}
