<?php

namespace Modules\Item\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CashagenteRequest extends FormRequest
{
     
    public function authorize()
    {
        return true; 
    }
 
    public function rules()
    { 
        
        $id = $this->input('id');
        return [
             
            'user_id' => [
                'required',
            ],
            'saldo1' => [
                'required',
            ],
            'saldo2' => [
                'required',
            ],
            'saldo3' => [
                'required',
            ]



        ];

    }
}
