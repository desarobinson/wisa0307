<?php

namespace Modules\Item\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CashagenteCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {

            return [
                'id' => $row->id,
                'user_id' => $row->user_id,
                'saldo1' => $row->saldo1,
                'saldo2' => $row->saldo2,
                'saldo3' => $row->saldo3,
                'estado' => $row->estado,
                'id_cash' => $row->id_cash,
                'fechaopen' => $row->created_at->format('Y-m-d'),
                'horaopen' => $row->created_at->format('H:i:s'),
                'created_at' => $row->created_at->format('Y-m-d H:i:s'),
                'updated_at' => $row->updated_at->format('Y-m-d H:i:s'),
            ];
        });

    }
    
}
