<?php

namespace Modules\Item\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Item\Models\Cashagente;
use Modules\Item\Models\Agente;
use Modules\Item\Models\Nombreagente;
use Modules\Item\Http\Resources\CashagenteCollection;
use Modules\Item\Http\Resources\CashagenteResource;
use Modules\Item\Http\Requests\CashagenteRequest;
use App\Models\Tenant\User;
use App\Models\Tenant\Company;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Log;
class CashagenteController extends Controller
{

    public function index()
    {
        return view('item::cashagentes.index');
    }


    public function columns()
    {
        return [
            'id' => 'Fecha Apertura',
           
        ];
    }

    public function records(Request $request)
    {
        $records = Cashagente::where($request->column, 'like', "%{$request->value}%")
                            ->latest();

        return new CashagenteCollection($records->paginate(config('tenant.items_per_page')));
    }
    public function tables() {
        $user = auth()->user();
        $type = $user->type;
        $users = array();
        $agente = array();
        $agente = User::where('type', 'seller')->get();
        $agente ->push($agente);

        switch($type)
        {
            case 'admin':
                $users = User::where('type', 'seller')->get();
                $users->push($user);
                break;
            case 'seller':
                $users = User::where('id', $user->id)->get();
                break;
        }

        return compact('users', 'user'.'agente');

    }
    public function report($cash1) {

        $cash = Cashagente::findOrFail($cash1);
        $company = Company::first();
        $agente = Agente::all();
        $agente1= Nombreagente::where('id', '1')->get();
        $agente2= Nombreagente::where('id', '2')->get();
        $agente3= Nombreagente::where('id', '3')->get();
        Log::info($agente1);
        $sum1= Agente::where('id_cash', $cash1)->where('tipo', 'retiro')->where('banks', 1)->get();
        $sum11= Agente::where('id_cash', $cash1)->where('tipo', 'deposito')->where('banks', 1)->get();

        $sum2= Agente::where('id_cash', $cash1)->where('tipo', 'retiro')->where('banks', 2)->get();
        $sum22= Agente::where('id_cash', $cash1)->where('tipo', 'deposito')->where('banks', 2)->get();

        $sum3= Agente::where('id_cash', $cash1)->where('tipo', 'retiro')->where('banks',3)->get();
        $sum33= Agente::where('id_cash', $cash1)->where('tipo', 'deposito')->where('banks', 3)->get();

        set_time_limit(0);
       
        $pdf = PDF::loadView('tenant.cashagente.report_pdf', compact("cash", "company", "agente","sum1","sum11","sum2","sum22","sum3","sum33","agente1","agente2","agente3"));

        $filename = "Reporte_POS";

        return $pdf->stream($filename.'.pdf');
    }
    public function record($id)
    {
        $record = Cashagente::findOrFail($id);

        return $record;
    }

    public function store(CashagenteRequest $request)
    {
        $id = $request->input('id');
        $agente = Cashagente::firstOrNew(['id' => $id]);

        $agente->fill($request->all());
       
        $agente->save();


        return [
            'success' => true,
            'message' => ($id)?'Agente aperturado editada con éxito':'Agente aperturado registrada con éxito',
            'data' => $agente

        ];

    }

    public function destroy($id)
    {
        try {

            $agente = Cashagente::findOrFail($id);
            $agente->delete();

            return [
                'success' => true,
                'message' => 'Categoría eliminada con éxito'
            ];

        } catch (Exception $e) {

            return ($e->getCode() == '23000') ? ['success' => false,'message' => "La categoría esta siendo usada por otros registros, no puede eliminar"] : ['success' => false,'message' => "Error inesperado, no se pudo eliminar la categoría"];

        }

    }
    public function cerrar($id)
    {
        try {

            $agente = Cashagente::findOrFail($id);
            $agente->estado = 2;
            $agente->save();

            return [
                'success' => true,
                'message' => 'Caja cerrada con exito'
            ];

        } catch (Exception $e) {

            return ($e->getCode() == '23000') ? ['success' => false,'message' => "La categoría esta siendo usada por otros registros, no puede eliminar"] : ['success' => false,'message' => "Error inesperado, no se pudo eliminar la categoría"];

        }

    }




}
