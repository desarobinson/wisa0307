<?php

namespace Modules\Item\Models;

use App\Models\Tenant\Item;
use App\Models\Tenant\ModelTenant;

class Cashagente extends ModelTenant
{

    protected $fillable = [ 
        'user_id',
        'saldo1',
        'saldo2',
        'saldo3',
        'estado',
        'id_cash',

    ];
 
    public function items()
    {
        return $this->hasMany(Item::class);
    }
    // public function user()
    // {
    //     return $this->belongsTo(User::class);
    // }
 
    public function scopeWhereTypeUser($query)
    {
        $user = auth()->user();
        return ($user->type == 'seller') ? $query->where('user_id', $user->id) : null;
    }

}