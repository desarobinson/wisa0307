@extends('tenant.layouts.app')

@section('content')
 
    <tenant-inventarios-edit :resource-id="{{json_encode($resourceId)}}"></tenant-inventarios-edit>

@endsection