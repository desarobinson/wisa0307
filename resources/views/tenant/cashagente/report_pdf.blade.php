@php

$establishment = $cash;

$resultado1 = 0;
$resultado11 = 0;
$resultado2 = 0;
$resultado22 = 0;
$resultado3 = 0;
$resultado33 = 0;

foreach ($sum1 as $item) {
    $resultado1= $resultado1+$item['monto'];
}
foreach ($sum11 as $item) {
    $resultado11= $resultado11+$item['monto'];
}
foreach ($sum2 as $item) {
    $resultado2= $resultado2+$item['monto'];
}

foreach ($sum22 as $item) {
    $resultado22= $resultado22+$item['monto'];
}
foreach ($sum3 as $item) {
    $resultado3= $resultado3+$item['monto'];
}
foreach ($sum33 as $item) {
    $resultado33= $resultado33+$item['monto'];
}
@endphp
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="application/pdf; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Reporte POS</title>
        <style>
            html {
                font-family: sans-serif;
                font-size: 12px;
            }

            table {
                width:33%;
                border-spacing: 0;
                border: 1px solid black;
            }

            .celda {
               
                padding: 5px;
                border: 0.1px solid black;
            }

            th {
                padding: 5px;
                
                border-color: #0088cc;
                border: 0.1px solid black;
            }

            .title {
                font-weight: bold;
                padding: 5px;
                font-size: 20px !important;
                text-decoration: underline;
            }

            p>strong {
                margin-left: 5px;
                font-size: 12px;
            }

            thead {
                font-weight: bold;
                background: #0088cc;
                color: white;
                text-align: center;
            }
            .td-custom { line-height: 0.1em; }

            .width-custom { width: 50% }
        </style>
    </head>
    <body>
        <div>
            <p  class="title"><strong>Reporte Caja agente</strong></p>
        </div>
        <div >


           
                     
                        <p><strong>Fecha y hora apertura: </strong>{{$cash->created_at}} </p>
                        
                        @if($cash->estado==1)
                        <p><strong>Estado de caja: </strong>Aperturado</p>
                        
                        @else
                        <p><strong>Estado de caja: </strong>Cerrado</p>
                        <p><strong>Fecha y hora cierre: </strong>{{$cash->updated_at}} </p>
                    @endif
                    <p><strong>Saldo Inicial: </strong></p>
                    <p><strong> @foreach($agente1 as $item)                          
                                    
                                    {{ $item['nombre'] }}
                                    
                                
                             @endforeach: {{$cash->saldo1}}</strong></p>
                    <p><strong> @foreach($agente2 as $item)                          
                                    
                                    {{ $item['nombre'] }}
                                    
                                
                             @endforeach: {{$cash->saldo2}}</strong></p>
                    <p><strong> @foreach($agente3 as $item)                          
                                    
                                    {{ $item['nombre'] }}
                                    
                                
                             @endforeach: {{$cash->saldo3}}</strong></p>
                    <p><strong>Montos de operación: </strong></p>
                    <p><strong>Saldo Final @foreach($agente1 as $item)                          
                                    
                                    {{ $item['nombre'] }}
                                    
                                
                             @endforeach : {{number_format($resultado11-$resultado1+$cash->saldo1, 1, ".", "")}} </strong></p>
                    
                             <p><strong>Saldo Final @foreach($agente2 as $item)                          
                                    
                                    {{ $item['nombre'] }}
                                    
                                
                             @endforeach : {{number_format($resultado22-$resultado2+$cash->saldo2, 2, ".", "")}} </strong></p>
                             <p><strong>Saldo Final @foreach($agente3 as $item)                          
                                    
                                    {{ $item['nombre'] }}
                                    
                                
                             @endforeach :  {{number_format($resultado33-$resultado3+$cash->saldo3, 2, ".", "")}} </strong></p>
                             
                    
                            
                  
        </div>
     
    </body>
</html>
